//
// Created by Auke on 13-6-2017.
//
#pragma once
#include <stdlib.h>
#include <assert.h>
#include "state.h"

typedef struct Heap {
    int size;
    int capacity;
    State* states;
} Heap;

void newHeap(Heap* hp);
int isEmptyHeap(Heap *hp);
void heapEmptyError();
void doubleHeapSize(Heap *hp);
void enqueue(Heap *hp, State s);
void enqueueUnique(Heap *hp, State s);
State dequeue(Heap *hp);
void upheap(Heap *hp, int n);
void downheap(Heap *hp, int n);
void heapSort(int size, State ar[]);
void swap(State *a, State *b);
void freeHeap(Heap *hp);
