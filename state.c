//
// Created by Auke on 13-6-2017.
//

#include <assert.h>
#include <stdlib.h>
#include "state.h"
#include "sandglasses.h"


void moveTime(State* s){
    //dTime is time untill next timeable time (Greater than 0);
    int dTime = min(s->minRemaining, s->maxRemaining);
    if(0 == s->minRemaining){
      dTime = s->maxRemaining;
    } else if(0 == s->maxRemaining){
      dTime = s->minRemaining;
    }
    assert(dTime > 0);

    //Move the time forward
    s->time += dTime;
    assert(s->time >= 0);

    //Update the sandglasses times
    s->minRemaining -= min(dTime, s->minRemaining);
    assert(s->minRemaining >= 0);
    s->maxRemaining -= min(dTime, s->maxRemaining);
    assert(s->maxRemaining >= 0);
}

void testState(State* state, int time){
  state->minRemaining = 0;
  state->maxRemaining = 0;
  state->time = time;
}

void newState(State* state, int minRemaining, int maxRemaining, int time){
  state->minRemaining = minRemaining;
  state->maxRemaining = maxRemaining;
  state->time = time;
}

State turnLeft(State base, int leftCapacity){
    State new = base;
    new.minRemaining = leftCapacity - base.minRemaining;
    moveTime(&new);
    return new;
}

State turnRight(State base, int rightCapacity){
    State new = base;
    new.maxRemaining = rightCapacity - base.maxRemaining;
    moveTime(&new);
    return new;
}

State turnBoth(State base, int leftCapacity, int rightCapacity){
    State new = base;
    new.minRemaining = leftCapacity - base.minRemaining;
    new.maxRemaining = rightCapacity - base.maxRemaining;
    moveTime(&new);
    return new;
}

int oldCompare(State a, State b){
  if(abs(a.minRemaining-a.maxRemaining) < abs(b.minRemaining - b.maxRemaining)){
    return 1;
  }
  if(abs(a.minRemaining-a.maxRemaining) > abs(b.minRemaining - b.maxRemaining)){
    return -1;
  }

  //Compare by a different field if needed
  if(a.time > b.time){
    return 1;
  }
  if(b.time > a.time){
    return -1;
  }

  return 0;
}

int compare(State a, State b){
  if(a.time > b.time){
    //1 for maxHeap, -1 for minHeap
    return 1;
//    return -1;
  }
  if(a.time < b.time){
    //-1 for maxHeap, 1 for minHeap
    return -1;
//    return 1;
  }

  //Compare by a different field if needed
  return 0;
}

int equalState(State a, State b){
  if( a.minRemaining == b.minRemaining &&
      a.maxRemaining == b.maxRemaining &&
      a.time == b.time){
    return 1;
  }
  return 0;
}