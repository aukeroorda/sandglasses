//
// Created by Auke on 14-6-2017.
//
#pragma once

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

//DEBUG toggles tests and debugging output
#ifndef DEBUG
#define DEBUG 0
#endif

int timeable(int leftCap, int rightCap, int goalTime);
