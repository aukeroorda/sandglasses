//
// Created by Auke on 14-6-2017.
//
#pragma once

#include "heap.h"

void printHeapArray(Heap* hp);
void printState(State* sp);
void testHeap();
void testMaxHeap();
void testMinHeap();
void testTimeable();
void test();