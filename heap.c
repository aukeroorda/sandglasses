//
// Created by Auke on 13-6-2017.
//
#include <stdio.h>
#include <string.h>
#include "heap.h"
#include "tests.h"
#include "sandglasses.h"


void newHeap(Heap* hp){
    hp->states = malloc(1*sizeof(State));
    assert(NULL != hp->states);
    hp->size = 0;
    hp->capacity = 1;
}

int parentIndex(int n){
  return (n-1) / 2;
}

int leftChildIndex(int n){
  return 2*n + 1;
}

int rightChildIndex(int n){
  return 2*n + 2;
}

int isEmptyHeap(Heap *hp){
    return (0 == hp->size);
}

void heapEmptyError(){
    printf("Heap is empty\n");
    EXIT_FAILURE;
}
void doubleHeapSize(Heap *hp){
    int newSize = 2*hp->capacity;
    hp->states = realloc(hp->states, newSize * sizeof(State));
    assert(NULL != hp->states);
    hp->capacity = newSize;
}

void enqueue(Heap *hp, State s){
  if (hp->size == hp->capacity){
      doubleHeapSize(hp);
  }
//Copy the State s into the array.
  memcpy(&(hp->states[hp->size]), &s, sizeof(State));
  upheap(hp, hp->size);
  hp->size = hp->size+1;
}

//Recursive search form the root. Works, but slow for large heaps.
int heapSearchUntill(Heap *hp, State s, int index, int maxIndex){
  //Base case
  if(index >= min(hp->size, maxIndex)){
    return 0;
  }
  if(equalState(hp->states[index], s)){
    return 1;
  }
  if(compare(hp->states[index], s) != 1){
    return 0;
  }
  return heapSearchUntill(hp, s, leftChildIndex(index), maxIndex)
        +heapSearchUntill(hp, s, rightChildIndex(index), maxIndex);
}

void enqueueUnique(Heap *hp, State s){
  int r = 0;
  r = heapSearchUntill(hp, s, 0, 4);
  if(0 == r){
    enqueue(hp, s);
  }
}

State dequeue(Heap *hp){
    if(isEmptyHeap(hp)){
        heapEmptyError();
    }
    State s;
    memcpy(&s, &(hp->states[0]), sizeof(State));
    hp->size--;
    memcpy(&(hp->states[0]), &(hp->states[hp->size]), sizeof(State));
    downheap(hp, 0);
    return s;
}

void upheap(Heap *hp, int n){
    if (n>0 && compare(hp->states[n], hp->states[parentIndex(n)]) > 0){
        swap(&(hp->states[n]), &(hp->states[parentIndex(n)]));
        upheap(hp, parentIndex(n));
    }
}

void downheap(Heap *hp, int n){
    int indexMax = n;
    if(hp->size < 2*n + 1) {
        return;
    }

    if(compare(hp->states[leftChildIndex(n)], hp->states[n]) > 0){
        indexMax = leftChildIndex(n);
    }

    if (hp->size > 2*n+1 && (compare(hp->states[rightChildIndex(n)], hp->states[indexMax]) > 0)) {
        indexMax = rightChildIndex(n);
    }

    if(indexMax != n){
        swap(&(hp->states[n]), &(hp->states[indexMax]));
        downheap(hp, indexMax);
    }
}

void heapSort(int size, State ar[]){
    Heap heap;
    newHeap(&heap);
    for(int i=0; i<size; i++){
        enqueue(&heap, ar[i]);
    }
    for(int i=0; i<size; i++){
        ar[size-i-1] = dequeue(&heap);
    }
}


void swap(State *a, State *b){
    State p = *a;
    *a = *b;
    *b = p;
}

void freeHeap(Heap *hp){
    free(hp->states);
}