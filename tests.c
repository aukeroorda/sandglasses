//
// Created by Auke on 14-6-2017.
//

#include <stdio.h>
#include "tests.h"
#include "heap.h"
#include "sandglasses.h"


void test(){
//  testHeap();
  testTimeable();

}


void printHeapArray(Heap* hp){
  printf("Heap: .size=%d .capacity=%d\n", hp->size, hp->capacity);
  for(int i = 0; i < hp->size; i++){
    printState(&hp->states[i]);
  }
}

void printState(State* st){
  printf("State: .lr:%2d .rr:%2d .t:%7d\n", st->minRemaining, st->maxRemaining, st->time );
}

void testHeap(){
  State a, b;
  testState(&a, 1);
  testState(&b, 0);
  if(compare(a, b)>0){
    //maxheap
    testMaxHeap();
  } else {
    //minheap
    testMinHeap();
  }
}

void testMaxHeap(){
  Heap hp;
  newHeap(&hp);

  State a, b, c, d, e;
  testState(&a, 1);
  testState(&b, 2);
  testState(&c, 3);
  testState(&d, 4);
  testState(&e, 5);

  assert(1 == hp.capacity);
  enqueueUnique(&hp, a);
  State f = dequeue(&hp);
  assert(equalState(a, f));
  assert(0 == hp.size);
  enqueueUnique(&hp, e);
  enqueueUnique(&hp, b);
  assert(2 == hp.capacity);
  State g = dequeue(&hp);
  assert(equalState(e, g));
  enqueueUnique(&hp, d);
  enqueueUnique(&hp, c);
  enqueueUnique(&hp, g);

  assert(equalState(dequeue(&hp), e));
  assert(equalState(dequeue(&hp), d));
  assert(equalState(dequeue(&hp), c));
  assert(equalState(dequeue(&hp), b));

  newHeap(&hp);
  if(DEBUG) {
    printHeapArray(&hp);
  }

  State h, i, j, k;
  testState(&h, 1);
  testState(&i, 5);
  testState(&j, 4);
  testState(&k, 3);

  State states[] = {h, i, j, k};
  State expectedStates[] = {i, j, k, h};

  for(int i=0; i< sizeof(states)/sizeof(State); i++){
    enqueueUnique(&hp, states[i]);
    if(DEBUG) {
      printHeapArray(&hp);
    }
  }

  for(int i=0; i< sizeof(states)/sizeof(State); i++){
    State a = dequeue(&hp);
    State b = expectedStates[i];
    assert(equalState(a, b));
  }

  freeHeap(&hp);
  if(DEBUG) {
    printf("Test maxHeap passed.\n");
  }
}

void testMinHeap(){
  Heap hp;
  newHeap(&hp);

  State a, b, c, d, e;
  testState(&a, 1);
  testState(&b, 2);
  testState(&c, 3);
  testState(&d, 4);
  testState(&e, 5);

  assert(1 == hp.capacity);
  enqueueUnique(&hp, a);
  State f = dequeue(&hp);
  assert(equalState(a, f));
  assert(0 == hp.size);
  enqueueUnique(&hp, e);
  enqueueUnique(&hp, b);
  assert(2 == hp.capacity);
  State g = dequeue(&hp);
  assert(equalState(b, g));
  enqueueUnique(&hp, d);
  enqueueUnique(&hp, c);
  enqueueUnique(&hp, g);

  assert(equalState(dequeue(&hp), b));
  assert(equalState(dequeue(&hp), c));
  assert(equalState(dequeue(&hp), d));
  assert(equalState(dequeue(&hp), e));

  newHeap(&hp);
  if(DEBUG) {
    printHeapArray(&hp);
  }

  State h, i, j, k;
  testState(&h, 1);
  testState(&i, 5);
  testState(&j, 4);
  testState(&k, 3);

  State states[] = {h, i, j, k};
  State expectedStates[] = {h, k, j, i};

  for(int i=0; i< sizeof(states)/sizeof(State); i++){
    enqueueUnique(&hp, states[i]);
    if(DEBUG) {
      printHeapArray(&hp);
    }
  }

  for(int i=0; i< sizeof(states)/sizeof(State); i++){
    State a = dequeue(&hp);
    State b = expectedStates[i];
    assert(equalState(a, b));
  }

  freeHeap(&hp);
  if(DEBUG) {
    printf("Test minHeap passed.\n");
  }
}

void testTimeable(){
  //Should be timeable
  assert(timeable(19, 47, 121));
  assert(timeable(133, 347, 509));
  assert(timeable(133, 340, 2650));
  assert(timeable(1, 3, 6));
  assert(timeable(5, 1, 8));
  assert(timeable(8, 3, 0));
  assert(timeable(1, 100000, 100000));

  //Should not be timeable
  assert(!timeable(17, 57, 106));
  assert(!timeable(18, 75, 226));
  assert(!timeable(133, 347, 611));
}
