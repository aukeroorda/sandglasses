//
// Created by Auke on 13-6-2017.
//
#pragma once

typedef struct State {
    int minRemaining;
    int maxRemaining;
    int time;
} State;

void testState(State* state, int time);
void newState(State* state, int minRemaining, int maxRemaining, int time);
void moveTime(State* s);
State turnLeft(State base, int leftCapacity);
State turnRight(State base, int rightCapacity);
State turnBoth(State base, int leftCapacity, int rightCapacity);
int compare(State a, State b);
int equalState(State a, State b);