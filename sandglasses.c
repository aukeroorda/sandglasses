#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "heap.h"
#include "state.h"
#include "tests.h"
#include "sandglasses.h"

//TODO:
//Improve modcheck


int totalTime = 0;

int minNotZero(int a, int b){
  if(0==a){
    return b;
  }
  if(0==b){
    return a;
  }
  return min(a, b);
}

//Source: https://stackoverflow.com/a/19738969/3684659
int gcd(int a, int b)
{
  int temp;
  while (b != 0)
  {
    temp = a % b;

    a = b;
    b = temp;
  }
  return a;
}

/**
 * Checks whether the remaining time to measure is a multiple of any sandglass which capacity is zero, or subtracts the current timer and checks.
 * @param state
 * @param goalTime
 * @return
 */
int modCheck(State state, int minCap, int maxCap, int goalTime){
  int remainingTime = goalTime - state.time;
  if(0 == state.minRemaining){
    //Left glass is empty

    if( 0 == remainingTime%minCap ||
        0 == (remainingTime - state.maxRemaining)%minCap ||
        0 == (remainingTime - state.maxRemaining)%maxCap){
      return 1;
    }

    if(0 != state.maxRemaining && 0 == remainingTime%state.maxRemaining){
      return 1;
    }
  }

  if(0 == state.maxRemaining){
    //Right glass is empty

    if( 0 == remainingTime%maxCap ||
        0 == (remainingTime - state.minRemaining)%minCap ||
        0 == (remainingTime - state.minRemaining)%maxCap){
      return 1;
    }

    if(0 != state.minRemaining && 0 == remainingTime%state.minRemaining){
      return 1;
    }
  }

  if(0 == state.minRemaining && 0 == state.maxRemaining){
    //Both glasses are empty
    if(0 == remainingTime%(minCap+maxCap)){
        return 1;
    }
  }

  /* Is the goalTime a multiple of the current time?*/
  if(0 != totalTime && 0 == goalTime % totalTime){
    return 1;
  }

  return 0;
}

int specialStartingCases(int minCap, int maxCap, int goalTime) {/*special cases*/
  if(0 == maxCap%minCap){
    if(0 == (goalTime%minCap)){
      return 1;
    } else {
      return 0;
    }
  }

  /*Can they only make even numbers*/
  if(0 == minCap%2 && 0 == maxCap%2 && 1 == goalTime%2){
    return 0;
  }

  if(0 == goalTime%(minCap + maxCap) || 0 == goalTime%minCap || 0 == goalTime%maxCap ){
    return 1;
  }
  return -1;
}

/* The function timeable checks whether a given time can be determined
 * exactly by two sandglasses with given capacities.
 */

int timeable(int leftCap, int rightCap, int goalTime) {
  Heap heap;
  newHeap(&heap);

//  int div = gcd(leftCap, rightCap);
//  if(0 != goalTime%div){
//    printf("timeable: false  size:%6d capacity:%6d gcd %d\n", heap.size, heap.capacity, div);
//    freeHeap(&heap);
//    return 0;
//  }

  int minCap = min(leftCap, rightCap);
  int maxCap = max(leftCap, rightCap);

  State startingState;
  newState(&startingState, 0, 0, 0);

  int r = specialStartingCases(minCap, maxCap, goalTime);
  if(r != -1){
    if(DEBUG) {
      printf("timeable: true  size:%6d capacity:%6d specialStartingCases\n", heap.size, heap.capacity);
    }
    freeHeap(&heap);
    return r;
  }
  if(modCheck(startingState, minCap, maxCap, goalTime)){
    if(DEBUG) {
      printf("timeable: true  size:%6d capacity:%6d modCheck\n", heap.size, heap.capacity);
    }
    freeHeap(&heap);
    return 1;
  }

  State startBoth = turnBoth(startingState, minCap, maxCap);
  if (startBoth.time <= goalTime && (startBoth.time + minNotZero(startBoth.minRemaining, startBoth.maxRemaining) <= goalTime)) {
    enqueue(&heap, startBoth);
  }

  State currentState;
  while(!isEmptyHeap(&heap)) {
    currentState = dequeue(&heap);

    if (modCheck(currentState, minCap, maxCap, goalTime)) {
      if(DEBUG) {
        printf("timeable: true  size:%6d capacity:%6d modCheck\n", heap.size, heap.capacity);
      }
      freeHeap(&heap);
      return 1;
    }

      State tl = turnLeft(currentState, minCap);
      if (tl.time <= goalTime && (tl.time + minNotZero(tl.minRemaining, tl.maxRemaining) <= goalTime)) {
        enqueue(&heap, tl);
      }
      State tr = turnRight(currentState, maxCap);
      if (tr.time <= goalTime && (tr.time + minNotZero(tr.minRemaining, tr.maxRemaining) <= goalTime)) {
        enqueue(&heap, tr);
      }

    if((0 == currentState.minRemaining && 0 == currentState.maxRemaining)
       &&(currentState.minRemaining != maxCap-minCap
       || currentState.maxRemaining != minCap-maxCap)
            ){
      State tb = turnBoth(currentState, minCap, maxCap);
      if (tb.time <= goalTime && (tb.time + minNotZero(tb.minRemaining, tb.maxRemaining) <= goalTime)) {
        enqueue(&heap, tb);
      }
    }

  }
  if(DEBUG) {
    printf("timeable: false  size:%6d capacity:%6d\n", heap.size, heap.capacity);
  }
  freeHeap(&heap);
  return 0;
  
}

/* main performs a dialogue with the user. The user is asked to provide three numbers: 
 * the (positive) capacities of the sandglasses and the goal time (>= 0).
 * The program indicates whether the goal time can be determined exactly. 
 * The dialogue is ended by the input '0'.
 */

int main(int argc, char *argv[]){

  if(DEBUG) {
    //Run heap and state tests.
    test();
  }

  int cap1, cap2, goalTime;
  printf("give the sandglass capacities and the goal time: ");
  scanf("%d",&cap1);
  while ( cap1 > 0 ) {
    scanf("%d",&cap2);
    assert( cap2 > 0 );
    scanf("%d",&goalTime);
    assert( goalTime >= 0 );
    if ( timeable(cap1,cap2,goalTime) ) {
      printf("%d and %d can time %d\n", cap1, cap2, goalTime);
    } else {
      printf("%d and %d cannot time %d\n", cap1, cap2, goalTime);
    }
    printf("\ngive the sandglass capacities and the goal time: ");
    scanf("%d",&cap1);
  }  
  printf("good bye\n");
  return 0;
}
